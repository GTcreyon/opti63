#!/usr/bin/env python
"""Build opti63 from an input SWF file by applying patches."""

import subprocess
from os import environ
from flash_patcher import patcher as fp
from argparse import ArgumentParser
from pathlib import Path

parser = ArgumentParser(
    description="Build opti63 from an input SWF file by applying patches.",
)

parser.add_argument(
    "-i",
    "--input",
    type=Path,
    required=False,
    help="Input SWF file",
    default="2012.swf",
)
parser.add_argument(
    "-g",
    "--gameversion",
    type=str,
    required=False,
    help="Version of the game to be used, e.g. 2009, 2012",
    default="2012",
)
parser.add_argument(
    "-o",
    "--output",
    type=Path,
    required=False,
    help="Output path for the patched SWF",
    default="opti63.swf",
)
parser.add_argument(
    "--fps",
    type=str,
    required=False,
    help="[EXPERIMENTAL] Change the framerate cap. Causes speedup and glitches.",
    default="32",
)

args = parser.parse_args()

# Track whether the header needs to be changed.
edit_header = args.fps != "32"

if edit_header:
    filename = f".temp_{args.output}"
else:
    filename = args.output

environ["63VER"] = args.gameversion
fp.main(args.input, Path("patches"), Path(f"{args.gameversion}.stage"), filename)

if edit_header:
    subprocess.run(
        [
            "ffdec",
            "-header",
            "-set",
            "framerate",
            args.fps,
            filename,
            args.output,
        ],
        check=True,
    )