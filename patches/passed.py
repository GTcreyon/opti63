#!/usr/bin/env python
import json
from os import environ
from pathlib import Path

BOILERPLATE = """modList = []
creditList = []
addMod = function(name, ver, credits) {
    modList.push(name + ":" + ver)
    while (credits.length > 0) {
        var credit = credits.pop()
        var unique = true
        var i = 0
        while (i < creditList.length && unique) {
            unique = creditList[j] !== credit
            i++
        }
        if (unique) {
            creditList.push(credit)
        }
    }
    passed = "Mods installed: " + modList.join(", ") + "\\nCredits: " + creditList.join(", ")
}"""

match environ["63VER"]:
    case "2012":
        SPRITEID = "2126"
    case "2009":
        SPRITEID = "2110"
SCRPATH = Path(f"scripts/DefineSprite_{SPRITEID}/frame_1/DoAction.as")

with SCRPATH.open("r+") as f:
    if not f.readline().startswith(BOILERPLATE.partition("\n")[0]):
        f.seek(0)
        f.truncate()
        f.write(BOILERPLATE)

    with Path("../../modmeta.json").open("r") as j:
        modmeta = json.load(j)

    credits_info = '", "'.join(modmeta["credits"])
    f.write(f'\naddMod("{modmeta["name"]}", "{modmeta["version"]}", ["{credits_info}"])')

print(SCRPATH)
