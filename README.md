# opti63
## Overview
opti63 is a set of bug-fix and optimization patches for Super Mario 63. It aims to reduce sources of lag, and fix bugs impacting the game without disrupting glitches that are used intentionally in custom levels.

## Changes
### Optimizations
- Lazy item loading
	- When loading a custom level, items will not switch to their correct visual sprite until they become visible onscreen.
	- This dramatically reduces the overhead from loading a level, since changing an item's active frame is an expensive operation.

### Bugs fixed
- Collectibles don't respawn consistently in custom levels with splits.
	- This was due to an oversight in how collectible resetting works in the Level Designer.
	- Collectibles were only reset in the current room and all rooms prior, not later rooms.
	- If the player were to backtrack and then die or exit the level, collectibles in later rooms would not respawn in future attempts, rendering some levels unbeatable.
- The intro sequence on the 2009 version freezes if audio is not working properly.
    - This is because the intro sequence was synced with the music. If the music doesn't start, neither does the intro.

## Build
### Requirements
- [Riley's SWF Patcher](https://github.com/rayyaw/flash-patcher)
	- The `flash-patcher` utility must be accessible via the PATH environment variable.
	- opti63 has been designed and tested for compatibility with v5.1.3 of the Flash Patcher.
- [FFDec](https://github.com/jindrapetrik/jpexs-decompiler)
	- opti63 has been designed and tested for compatibility with v20.0.0 of FFDec.
- [Python](https://www.python.org/downloads)
- A clean copy of Super Mario 63, as a SWF file.
	- Either the 2009 or 2012 version will work, but take note of which you are using.
	- An executable bundle (.exe) will not work. You should either find a raw SWF version, or extract the SWF using an external tool such as [swf-unbundler](https://github.com/GTcreyon/swf-unbundler).
- [The contents of this repository.](https://gitlab.com/GTcreyon/opti63/-/archive/main/opti63-main.zip)

### Method
Run the command:
```bash
./build.py -i <PATH-TO-YOUR-SM63-SWF>.swf -g <VERSION>
```
- \<PATH-TO-YOUR-SM63-SWF> is the path to the clean Super Mario 63 SWF that you intend to patch
- \<VERSION> is the identifier for the version of the game that the SWF contains. This will be either 2009 or 2012. If in doubt, you most likely want 2012, since this is the most up-to-date version.

Optionally, you may specify an output file path using the `-o`/`--output` option.

For example...
```bash
./build.py -i 2012sm63.swf -g 2012 -o out.swf
```
...will use the SWF file `2012sm63.swf` in the current directory, applying the appropriate patches for the 2012 version, and save the result in `out.swf`, also in the current directory.
